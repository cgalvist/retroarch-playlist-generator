#!/bin/bash

# Set to wherever your RetroArch directory is (contains paylists folder, cores folder, etc.), typically in $HOME/.config/retroarch
RetroArchDir="$HOME/.config/retroarch"
# If you not use custom RetroArch directory change this to $HOME/.config/retroarch
RetroArchDirCustom="/media/cgt/Datos/Información/Programas/Juegos/RetroArch"
# Add your games folder here
RomsDir="/media/cgt/Datos/Información/Programas/Juegos"

# If you use core updater change this to $HOME/.config/retroarch/cores/
CoresDir="$RetroArchDirCustom/cores"

# Examples

# SNES
RomDirs[0]="SNES"
CoreLibs[0]="snes9x_libretro.so"
CoreNames[0]="Snes9x"
PlaylistNames[0]="Nintendo - Super Nintendo Entertainment System"
SupportedExtensions[0]="*.smc *.fig *.sfc *.gd3 *.gd7 *.dx2 *.bsx *.swc"

# NES
RomDirs[1]="NES"
CoreLibs[1]="nestopia_libretro.so"
CoreNames[1]="Nestopia"
PlaylistNames[1]="Nintendo - Nintendo Entertainment System"
SupportedExtensions[1]="*.nes"

# Game Boy Advance
RomDirs[2]="Game Boy Advance"
CoreLibs[2]="mgba_libretro.so"
CoreNames[2]="mGBA"
PlaylistNames[2]="Nintendo - Game Boy Advance"
SupportedExtensions[2]="*.gba *gbc"

# Sega Genesis
RomDirs[3]="Sega Genesis"
CoreLibs[3]="genesis_plus_gx_libretro.so"
CoreNames[3]="Genesis Plus GX"
PlaylistNames[3]="Sega - Mega Drive - Genesis"
SupportedExtensions[3]="*.bin"

# Nintendo 64
RomDirs[4]="Nintendo 64"
CoreLibs[4]="mupen64plus_next_libretro.so"
CoreNames[4]="Mupen64Plus"
PlaylistNames[4]="Nintendo - Nintendo 64"
SupportedExtensions[4]="*.z64 *.v64 *.rom"

# No need to edit anything beyond this point, unless you don't want it to delete files, go down.

clear

echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "!!                                                                            !!"
echo "!!  WARNING: Continuing to run this program will delete your playlist files.  !!"
echo "!!     If you would like to back them up, do so now, or RIP in pepperonis     !!"
echo "!!                             to your playlists.                             !!"
echo "!!                                                                            !!"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo

read -p "Press [Enter] key to continue..."

# This deletes all playlists (only .lpl files)
rm -f "$RetroArchDir"/playlists/*.lpl

index=0

while [ -n "${RomDirs[$index]}" ]
do
    echo "======================================================================"
    echo "${PlaylistNames[$index]}"
    echo "======================================================================"
    
    cd "$RomsDir/${RomDirs[$index]}/Roms"
    
    # PlayList path
    TMP_JSON="$RetroArchDir/playlists/${PlaylistNames[$index]}.tmp"
    PL_PATH="$RetroArchDir/playlists/${PlaylistNames[$index]}.lpl"
    
    # Make empty playlist
    echo -e "{\"version\": \"1.2\",\"default_core_path\": \"\",\"default_core_name\": \"\",\"label_display_mode\": 0,\"right_thumbnail_mode\": 0,\"left_thumbnail_mode\": 0,\"items\": [" > "$TMP_JSON"    
                
    echo -e "\nAdding files to playlist...\n"
    
    # add roms data in playlist
    for gameFile in ${SupportedExtensions[$index]}
    do
        if [ -f "$gameFile" ]; then
            crc32File=$(crc32 "$gameFile")
            (
              echo -e "{"
              echo -e "\"path\": \"$RomsDir/${RomDirs[$index]}/Roms/$gameFile\","
              echo -e "\"label\": \"${gameFile%.*}\","
              echo -e "\"core_path\": \"$CoresDir/${CoreLibs[$index]}\","
              echo -e "\"core_name\": \"${CoreNames[$index]}\","
              echo -e "\"crc32\": \"${crc32File^^}|crc\","
              echo -e "\"db_name\": \"${PlaylistNames[$index]}\""
              echo -e "}"
              echo ","
            ) >> "$TMP_JSON"
        fi
    done
    
    # delete last JSON comma
    sed -i '$ d' "$TMP_JSON"
    
    # close JSON array and object
    echo -e "]}" >> "$TMP_JSON"
    
    # Make JSON file pretty
    jq . "$TMP_JSON" > "$PL_PATH"
    
    # Delete temp file
    rm -f "$TMP_JSON"
    
    echo "$PL_PATH"
    
    ((index++))
done

exit 0

