# RetroArch Playlist generator

Script for generate RetroArch Playlists in JSON format for Linux systems.

This will take roms and add them to a playlist for RetroArch to use when they don't auto add because of CRC checking.

Work based in u/ShiftyAxel on Reddit for Windows.
Bash code based in [LPLmaker](https://github.com/jsbursik/LPLmaker) made by jsbursik.

## Folders structure

Example:

```
Games
└───SNES
│   └───Roms
│       │   game_1
│       │   game_2
│       │   ...
│   
└───NES
│   └───Roms
│       │   game_1
│       │   game_2
│       │   ...
│   
...
```

## Instructions (Ubuntu)

* Download requirements:

```shell
sudo apt-get update
sudo apt-get install libarchive-zip-perl jq
```

* Add script permissions:
```shell
sudo chmod +x plGen.sh
```

* Edit the script with your configuration

* Execute the script with the command `./plGen.sh`

* Enjoy!
